#include <string>
#include <vector>

#include <fstream>
#include <sstream>
#include <iostream>
#include <random>
#include <time.h>
#include <boost/dynamic_bitset.hpp>

using namespace boost;
using namespace std;
typedef vector<vector<int>> dag_t;


dag_t random_DAG(mt19937 &generator, uniform_int_distribution<int> &ud, bernoulli_distribution &bd){
  int nb_nodes = ud(generator);
  dag_t dag(nb_nodes, vector<int>());
  for (int node = 0; node < nb_nodes - 1; ++node){
    for (int neigh = node+1; neigh < nb_nodes; ++neigh){
      if (bd(generator)) dag[node].push_back(neigh);
    }
  }
  return dag;
}


//Helper function for the next function
dynamic_bitset<> compute_transitive_closure_recurse(const dag_t &dag, int node, vector<dynamic_bitset<>> &tc){
  if (tc[node][node]) return tc[node];
  tc[node].set(node);
  for(int neigh: dag[node]){
    tc[node] |= compute_transitive_closure_recurse(dag, neigh, tc);
  }
  return tc[node];
}


/* INPUT: a DAG with n nodes.
   OUTPUT: for node i, tc[i] is a bitset of length n whose j-th entry is 1 iff j is accessible from i.
 */
vector<dynamic_bitset<>> compute_transitive_closure(const dag_t &dag){
  vector<dynamic_bitset<>> tc(dag.size(), dynamic_bitset<>(dag.size()));
  for (int node = 0; node < static_cast<int>(dag.size()); ++node){
    compute_transitive_closure_recurse(dag, node, tc);
  }
  return tc;
}


// More or less this https://en.wikipedia.org/wiki/Transitive_reduction#Computing_the_reduction_using_the_closure
dag_t transitive_reduction(dag_t dag){
  dag_t tr(dag.size(), vector<int>());
  vector<dynamic_bitset<>> tc = compute_transitive_closure(dag);
  for(int node = 0; node < static_cast<int>(dag.size()); ++node){
    for(int &neigh: dag[node]){
      bool in_tr = true;
      for(int neigh2: dag[node]){
        if (neigh == neigh2) continue;
        if (tc[neigh2][neigh]){ in_tr = false; break;}
      }
      if (in_tr) tr[node].push_back(neigh);
    }
  }
  return tr;
}


string DAG_to_string(dag_t &dag){
  string result = to_string(dag.size()) + "\n";
  for(int node=0; node<static_cast<int>(dag.size()); ++node){
    result += to_string(node) + " ";
    for (auto &neigh: dag[node]) result += to_string(neigh) + " ";
    result.pop_back(); result += "\n";
  }
  result += "\n";
  return result;
}


/* INPUT: ...
   OUTPUT: writes into a file the transitive reductions of nb random DAGs.
 */
void generate(int min_nodes, int max_nodes, float prob, int nb){
  random_device rd;
  mt19937 generator{rd()};
  uniform_int_distribution<int> ud(min_nodes, max_nodes);
  bernoulli_distribution bd(prob);
  dag_t dag;
  ofstream storage("generated_"+to_string(min_nodes)+"_"+to_string(max_nodes)+"_"+to_string(prob), ios_base::app);

  for (int n=1; n <= nb; ++n){
    dag = random_DAG(generator, ud, bd);
    dag = transitive_reduction(dag);
    storage << DAG_to_string(dag);
  }

  storage.close();
}

int main(int arc, char** argv){
  /* Use this to generate argv[4] transitive reductions of random DAGs (for the
     game, a DAG is winning iff its transitive reduction is). The DAGs will be
     stored in the file generated_argv[1]_argv[2]_argv[3].
   */
  generate(stoi(argv[1]), stoi(argv[2]), atof(argv[3]), stoi(argv[4]));
}
