# The game

The game is described in
[https://cstheory.stackexchange.com/questions/45679/lighting-up-all-elements-of-a-poset-by-toggling-upsets](https://cstheory.stackexchange.com/questions/45679/lighting-up-all-elements-of-a-poset-by-toggling-upsets),
but I will explain it here for completeness.

### Some notation

The game is played on Directed Acyclic Graphs (DAGs).  Let $`G = (V,E)`$ be a
DAG with nodes $`V`$ and directed edges $`E \subseteq V \times V`$. For an edge
of the form $`u \to v`$, we say that $`u`$ is a *child* of $`v`$ and that $`v`$
is a *parent* of $`u`$. A node $`u`$ is a *descendent* of $`v`$, noted $`u \leq
v`$, if we can reach $`v`$ from $`u`$ by following an arbitrary number of
directed edges, and in that case we also say that $`v`$ is an *ancestor* of
$`u`$ (noted $`v \geq u`$). So we consider here that a node is an ancestor and a
descendent of itself.

### Game description

A *state $`\bf s`$ of the game on $`G`$* is a subset of the nodes of $`G`$. We
say that the nodes in $`\bf s`$ are $`\mathsf{on}`$ and that those not
in $`\bf s`$ are $`\mathsf{off}`$.  A *move* of the game at state $`\bf s`$
consists of choosing one node $`u`$ of $`G`$ and then:

  - If all the ancestors of $`u`$ are $`\mathsf{off}`$ (remember that this
    includes $`u`$), then we can play an *on-move* on $`u`$, which turns all
the ancestors of $`u`$ to $`\mathsf{on}`$. In other words, we obtain the new
state $`{\bf s'} = {\bf s} \cup \{v \in V \mid v \geq u\}`$. 
  - If all the ancestors of $`u`$ are $`\mathsf{on}`$, then we can play an
    *off-move* on $`u`$, which turns all the ancestors of $`u`$ to
$`\mathsf{off}`$. In other words, we obtain the new state $`{\bf s'} = {\bf s}
\setminus \{v \in V \mid v \geq u\}`$. 
  - Else, we are not allowed to play on $`u`$.


A *sequence* $`S = {\bf s}_0, \ldots, {\bf s}_K`$ is a sequence of states such
that for $`1 \leq i \leq K`$, state $`{\bf s}_i`$ is obtained from state $`{\bf
s}_{i-1}`$ by playing a move. A sequence is *valid* if for every node $`u`$,
exactly one of the following is true:

  - Every time we played on $`u`$, it was to do an on-move;
  - Every time we played on $`u`$, it was to do an off-move;

A sequence $`S`$ is *winning* if it is valid, and the first state of $`S`$ is
$`\emptyset`$ (i.e., all nodes are initially $`\mathsf{off}`$), and the last
state of $`S`$ is $`V`$ (i.e., all nodes end up $`\mathsf{on}`$).

A DAG $`G`$ is *winning* if there exists a winning sequence.

### Example

The following sequence is winning (nodes that are $`\mathsf{on}`$ are colored).

![](examples/example.gif)

The sequence that is played is: on-move on 1, off-move on 3, on-move on 6,
off-move on 5, on-move on 2, off-move on 4, on-move on 0.  For instance, we
always played off-moves on 4 (we actually only played one, but in general we
might need to play more than one move on the same node).

### Mobius values

It is easy to determine for each node how many times we will need to play on
it.  Define the following quantity for every node of $`G`$ by bottom-up
induction: $`\mu(u) = 1 - \sum_{v \leq u} \mu(v)`$. We call this the *Mobius
value of node $`u`$*. In particular, we have $`\mu(u)=1`$ when $`u`$ has no
children. Then the following holds for any winning sequence $`S`$: for every
node $`u \in V`$, if $`\mu(u) > 0`$ then we have played exactly $`\mu(u)`$
on-moves on $`u`$; and if $`\mu(u) < 0`$ then we have played exactly
$`-\mu(u)`$ off-moves on $`u`$ (and if $`\mu(u)=0`$ then we have never played
on $`u`$).  Therefore, to find a winning sequence, we can focus on valid
sequences that are what we call *Mobius-compliant*.  A valid sequence $`S`$ is
Mobius-compliant if the following is true: for every node $`u`$:

  - If $`\mu(u) > 0`$ then we have played $`\leq \mu(u)`$ on-moves on $`u`$;
  - If $`\mu(u) < 0`$ then we have played $`\leq -\mu(u)`$ off-moves on $`u`$.



# Requirements

1. A C++ compiler (like g++)
2. The C++ code uses boost's library dynamic_bitsets. You can install boost with `sudo apt-get install libboost-all-dev`
3. To visualize winning sequences, you will need the program “dot” from graphviz (to draw graphs): `sudo apt install graphviz`
4. One Bash helper script uses the program “convert” to create animated gifs: `sudo apt install imagemagick`


# How to use on an arbitrary DAG

1. Compile with `g++ -Wall -O3 game.cpp`
2. Create a file that represents a DAG as follows: the nodes are consecutive
integers $`0 \ldots n`$. The first line of the file contains the number of
nodes $`n+1`$. Then there are $`n+1`$ consecutive lines, were line $`i`$ starts
with $`i`$ and then list the parents of node $`i`$, separated with spaces. For
instance the file examples/q9 represents the DAG depicted above.
3. Use `./a.out filepath_of__DAG` to play on this DAG. First, it draws the DAG
in tmp/state_initial.png. The nodes are displayed with their names (top) and
their Mobius values (bottom). If the DAG is winning then it draws a winning
sequence in tmp/state_0.png, tmp/state_1.png, ...,tmp/state_K.png (again with
Mobius values of the nodes).  If the DAG is losing it simply prints “LOST”.

For instance, `./a.out examples/some_DAG` should create PNGs representing the
winning sequence depicted in examples/some_DAG_winning_sequence.gif, while
`./a.out examples/Louis_Jachiet_counterexample_DAG` is losing.



# DAGs generated by a Sperner family

The DAG in examples/Louis_Jachiet_counterexample_DAG was found by [Louis
Jachiet](https://louis.jachiet.com/) and is losing (a simpler losing DAG can be
found in examples/simpler_counterexample).  However, we are interested into a
special family of DAGs, that I define now. Let $`E = \{0,\ldots,k\}`$. A
*[Sperner family](https://en.wikipedia.org/wiki/Sperner_family)*
$`\mathrm{spf}`$ is a set of subsets of $`E`$ such that any two sets in
$`\mathrm{spf}`$ are incomparable by inclusion.  Given a Sperner family
$`\mathrm{spf}`$, *the DAG generated by* $`\mathrm{spf}`$, written
$`\mathrm{UNIONS}(\mathrm{spf})`$ is the [Hasse
diagram](https://en.wikipedia.org/wiki/Hasse_diagram) of the following poset
$`(P,\preceq)`$:
 
  - The elements of $`P`$ are all the possible non-empty unions of the sets in $`\mathrm{spf}`$;
  - For two elements $`e,e'`$ in $`P`$, we have $`e \preceq e'`$ iff $`e \subseteq e'`$.

with the arrows in $`\mathrm{UNIONS}(\mathrm{spf})`$ starting from the smallest element.

### Example

Let $`E = \{0,1,2,3\}`$ and $`\mathrm{spf} = \{\{0,3\}, \{1,3\}, \{2,3\},
\{0,1,2\}\}`$.  We will see the sets as bitsets, with the $`i`$-th least
significant bit (right) being $`1`$ iff $`(i+1)`$ is in the set. So the SPF
becomes $`\mathrm{spf} = \{\{1001\}, \{1010\}, \{1100\}, \{0111\}\}`$.  Then
$`\mathrm{UNIONS}(\mathrm{spf})`$ is the following DAG:

![](examples/UNIONS_SPF.png)

Note that this is the same DAG as the one previously drawn (they are
isomorphic), so it is winning.  The conjecture is that for every Sperner family
$`\mathrm{spf}`$, the DAG $`\mathrm{UNIONS}(\mathrm{spf})`$ is winning.

### Generating Sperner families

To test this conjecture we will generate all Sperner families for $`1 \leq k
\leq 6`$. I have [another project](https://gitlab.com/Gruyere/sperner-families-generator) for that. You need to clone it in a
directory “generated” under the main directory:

  - `git clone git@gitlab.com:Gruyere/sperner-families-generator.git generated`

Read the README and generate what we call there “nondegenerate inequivalent Sperner families”.

### Play on DAGs generated by Sperner families

Once you have generated some nondegenerate inequivalent Sperner families, you can play on a specific
Sperner family as follows:

  1. In game.cpp, comment preprocessor variable PLAY_AND_DRAW_ON_ONE_DAG and uncomment PLAY_AND_DRAW_ON_ONE_SPF
  2. Compile: `g++ -Wall -O3 game.cpp`
  3. Use `./a.out k l j` to play on the DAG generated by the spf stored on line
$`j`$ of the file generated/sperner/all_ineq_nondegenerate/k/j. As before, it
draws the DAG in tmp/state_init.png. Then if it is winning it draws the winning
sequence in tmp/state_XX. If it is losing it will tell you.

For instance, `./a.out 6 5 53` should put in tmp/ the successive states of a winning sequence, that when concatenated give the gif in example_graphs/k6l5j53_winning_sequence.gif

You can also uncomment the right preprocessor variables in game.cpp to play on all the DAGs generated by all the SPFs in some file.


# Play on randomly generated DAGs

Use the code in random_DAGs to generate some DAGs at random. This actually only
generates their transitive reductions
([https://en.wikipedia.org/wiki/Transitive_reduction](https://en.wikipedia.org/wiki/Transitive_reduction)),
because a DAG is winnable if and only if its transitive reduction is.  Then
uncomment the right preprocessor variable in game.cpp to either play on all the
DAGs stored in the file, or on one in particular.

# Acknowledgments

This game was invented with [Antoine Amarilli](http://a3nm.net/). If our conjecture that all the DAGs generated by Sperner families were to be true, this would solve an important open problem in the field of probabilistic databases; see [these slides](http://mikael-monet.net/slides/dbai2019.pdf) or [this paper](https://arxiv.org/abs/1912.11864).
Louis Jachiet found the counter-example DAG (but this DAG is not generated by any SPF).
