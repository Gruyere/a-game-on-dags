#include <string>
#include <vector>
#include <map>
#include <list>
#include <boost/dynamic_bitset.hpp>

#include <boost/functional/hash.hpp>
#include <unordered_set>
#include <queue>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <iostream>

namespace std {  // This comes from https://github.com/boostorg/dynamic_bitset/issues/34, because I needed a dynamic_bitset to have a hash function to put it in an unordered set
  template<> struct hash<boost::dynamic_bitset<>> {
    std::size_t operator()(const boost::dynamic_bitset<>& bs) const {
      std::string h;
      boost::to_string(bs, h);
      return std::hash<std::string>{}(h);
    }
  };
}
using namespace std;
using namespace boost;

typedef uint_fast16_t myint;
typedef vector<vector<int>> dag_t;
typedef pair<dynamic_bitset<>, vector<int>> stateAndMoves_t;


/* INPUT: filestream containing DAGs. DAGs are separated by empty lines. One
   DAG is encoded as follows: first line: number of nodes. Next lines:
   source_nodes and the children of source_node (adjacency list). So looks like
   this:
   n
   a b c d f (a and the neighbors of node a)
   e g h    (e and the neighbors of node e)
   ...      (n lines in total)
   OUTPUT: The next DAG stored in the filestream. dag[i] contains the list (vector) of the children of node i.
 */
dag_t read_next_dag(ifstream &ifile){
  int source_node;
  string line, tmp;
  getline (ifile,line);
  dag_t dag(stoi(line));
  while ( getline (ifile,line) && line.length()!=0 ){
    vector<int> neighbors;
    istringstream iss(line);
    iss >> tmp;
    source_node = stoi(tmp);
    for(string s; iss >> s; ){
      neighbors.push_back(stoi(s));
    }
    dag[source_node] = neighbors;
    }
  return dag;
}


/* INPUT: a DAG given by its adjacency list.
   OUTPUT: the adjacency list of the DAG where we reversed all the edges
  */
dag_t inverse_dag(const dag_t &dag){
  dag_t inv(dag.size(), vector<int>());
  for (int i = 0; i< static_cast<int>(dag.size()); ++i){
    for (int neigh: dag[i]){
    inv[neigh].push_back(i);
    }
  }
  return inv;
}


/* INPUT: DAG and state (0 or 1 for each node) and Mobius values of each node.
   OUTPUT: graphviz code to draw the dag. Node orange if its state is 1.
           Label of a node is the name of the node (on top), and its Mobius value (at the bottom).
 */
string dag_to_dot_for_DAG(const dag_t &dag, const dynamic_bitset<> &state, const vector<int> &mobius){
  string dotstring = "digraph G {\nrankdir = BT;\nnode [shape=Mrecord];\n";
  dotstring += "legend [label=\"{nodeId|Mobius value}\"]\n";
  //nodes
  for(int i = 0; i< static_cast<int>(dag.size());++i){
    string label = "{" + to_string(i) + "|" + to_string(mobius[i]) + "}";
    dotstring += "n_" + to_string(i) + " [label=\"" + label + "\"";
    if (state[i]) dotstring += ", fillcolor = \"darkorange\", style=filled";
    dotstring += "];\n";
  }
  //edges
  for(int i = 0; i< static_cast<int>(dag.size());++i){
    for(int neigh: dag[i]){
    dotstring += "n_" + to_string(i) + " -> n_" + to_string(neigh) +";\n";
    }
  }

  dotstring += "}";
  return dotstring;
}


/* INPUT: the DAG UNIONS(spf) for an SPF on a base set of k+1 elements,
          the state of the DAG (0 or 1 for each node) and Mobius values of each node, and the original names of the nodes.
   OUTPUT: dagviz code to draw the dag. Node orange if its state is 1.
           Label of a node is the set (on top, as a bitset), and its Mobius value (at the bottom).
 */
string dag_to_dot_for_SPF(int k, const dag_t &dag, const dynamic_bitset<> &state, const vector<int> &mobius, const map<int,myint> &original_names){
  string dotstring = "digraph G {\nrankdir = BT;\nnode [shape=Mrecord];\n";
  dotstring += "legend [label=\"{bitset|Mobius value}\"]\n";
  //nodes
  for(int i = 0; i< static_cast<int>(dag.size());++i){
    string bitset_string;
    to_string(dynamic_bitset<>(k+1, original_names.at(i)), bitset_string);
    string label = "{" + bitset_string + "|" + to_string(mobius[i]) + "}";
    dotstring += "n_" + to_string(i) + " [label=\"" + label + "\"";
    if (state[i]) dotstring += ", fillcolor = \"darkorange\", style=filled";
    dotstring += "];\n";
  }
  //edges
  for(int i = 0; i< static_cast<int>(dag.size());++i){
    for(int neigh: dag[i]){
    dotstring += "n_" + to_string(i) + " -> n_" + to_string(neigh) +";\n";
    }
  }

  dotstring += "}";
  return dotstring;
}


//Helper function for the next function
dynamic_bitset<> compute_transitive_closure_recurse(const dag_t &dag, int node, vector<dynamic_bitset<>> &tc){
  if (tc[node][node]) return tc[node];
  tc[node].set(node);
  for(int neigh: dag[node]){
    tc[node] |= compute_transitive_closure_recurse(dag, neigh, tc);
  }
  return tc[node];
}


/* INPUT: a DAG with n nodes.
   OUTPUT: for node i, tc[i] is a bitset of length n whose j-th entry is 1 iff j is accessible from i.
 */
vector<dynamic_bitset<>> compute_transitive_closure(const dag_t &dag){
  vector<dynamic_bitset<>> tc(dag.size(), dynamic_bitset<>(dag.size()));
  for (int node = 0; node < static_cast<int>(dag.size()); ++node){
    compute_transitive_closure_recurse(dag, node, tc);
  }
  return tc;
}


/* INPUT: the descendants relation of a dag, a node, the nodes whose Mobius value we have already computed (seen), the Mobius values of those nodes (on line 1, mobius[node] holds the Mobius value only if seen[node] is true).
   OUTPUT: the Mobius value of this node.
 */
int compute_mobius_recurse(const vector<dynamic_bitset<>> &desc, int node, vector<int> &mobius, vector<bool> &seen){
  if (seen[node]) return mobius[node];
  mobius[node] = 1;
  for(int i = 0; i< static_cast<int>(desc[node].size()); ++i){
  mobius[node] -= (desc[node][i] && i != node) ? compute_mobius_recurse(desc, i, mobius, seen) : 0;
  }
  seen[node] = true;
  return mobius[node];
}


/* INPUT: the descendants relation of a dag.
   OUTPUT: mobius[i] is the Mobius value of node i.
 */
vector<int> compute_mobius(const vector<dynamic_bitset<>> &desc){
  vector<int> mobius(desc.size());
  vector<bool> seen(desc.size());
  for (int node = 0; node < static_cast<int>(desc.size()); ++node){
    compute_mobius_recurse(desc, node, mobius, seen);
  }
  return mobius;
}


/* INPUT: a state (0 or 1 for each node in the dag) and the ancestors of some node
   OUTPUT: if playing on this node is legal (i.e., all its ancestors have the same state as the node).
 */
bool legal_move(const dynamic_bitset<> &state, const dynamic_bitset<> &anc){
  if ((state & anc) == anc) return true; //in this case all the ancestors are on
  else if ((state & anc).none()) return true; //else, they should all be off
  else return false;
}


/* INPUT: the state of a node, its Mobius value and the number of
          moves we have already done on that node (positive for on-moves, negative for off-moves).
   OUTPUT: true if playing on that node would be Mobius-compliant.
 */
bool move_is_mobius_compliant(bool state_node, int mobius_node, int moves_node){
  if (!state_node && (mobius_node - moves_node > 0) ) return true;
  if (state_node && (mobius_node - moves_node < 0) ) return true;
  return false;
}


/* INPUT: the state of the game, a node, the descendent and ancestors relations.
   OUTPUT: true if we know for sure that playing on this node leads on a state that has no path to the target.
 */
bool will_be_stuck(const dynamic_bitset<> &state, int node, const vector<dynamic_bitset<>> &anc, const vector<dynamic_bitset<>> &desc, const vector<int> &mobius, const vector<int> &moves){
  dynamic_bitset<> strict_anc_node = anc[node]; strict_anc_node[node] = false;
  dynamic_bitset<> strict_desc_node = desc[node]; strict_desc_node[node] = false;
  bool cond = false;
  if (!state[node] && strict_desc_node.any() && (state & strict_desc_node) != strict_desc_node){ //we will perform an on-move on this node
                                                          //and there is a strict descendent of node that is off.
                                                          //then there must exist a descendent node2 of node that is on and such that all nodes
                                                          //between node2 and node are on and such that playing on node2 would be Mobius-compliant
    for (int node2 = 0; node2 < static_cast<int>(mobius.size()); ++node2){
      if (strict_desc_node[node2]){
        dynamic_bitset<> in_between = strict_desc_node & anc[node2];
        if ( ((state & in_between) == in_between) && (mobius[node2] - moves[node2] < 0) ){cond = true; break;}
      }
    } 
    if (!cond) return true;
  }

  cond = false;
  if (state[node]){//we will perform, an off-move on this node. We check that there exists a strict descendent node2 of node that is off, with all nodes between node and node2 being off and such that playing on node2 would be Mobius-compliant
    for (int node2 = 0; node2 < static_cast<int>(mobius.size()); ++node2){
       if (strict_desc_node[node2]){
        dynamic_bitset<> in_between = strict_desc_node & anc[node2];
        if ( (state & in_between).none() && (mobius[node2] - moves[node2] > 0) ){cond = true; break;}
      }
    }
    if (!cond) return true;
  }
  return false;
}


/* INPUT: a state reachable from the all_off state and such that its negation is also reachable from the all-off state,
          and a map indicating the predecessor state for each state.
   OUTPUT: a list with winning_sequence[i] being the i-th state in the winning sequence (from the all-off state to the all-on state).
 */
list<dynamic_bitset<>> reconstruct_winning_sequence(const dynamic_bitset<> &state, const map<dynamic_bitset<>,dynamic_bitset<>> &pred){
  dynamic_bitset<> current_state = state;
  list<dynamic_bitset<>> winning_sequence;
  winning_sequence.push_front(state);
  //first half of the sequence
  while ( current_state.any() ){
    current_state = pred.at(current_state);
    winning_sequence.push_front(current_state);
  }
  //second half of the sequence
  current_state = ~state;
  while ( current_state.any() ){
    current_state = pred.at(current_state);
    winning_sequence.push_back(~current_state);
  }
  return winning_sequence;
}


/* INPUT: a state and the moves that let us to this state, a node and its ancestors.
   OUTPUT: the state and moves that we obtain by playing on this move.
   ASSUMES: playing on this move is legal and Mobius-compliant.
 */
stateAndMoves_t next_stateAndMoves(const stateAndMoves_t &stateAndMoves, int node, const dynamic_bitset<> &node_anc){
  dynamic_bitset<> next_state = stateAndMoves.first ^ node_anc;//Isn't it beautiful?
  vector<int> next_moves = stateAndMoves.second;
  next_moves[node] += stateAndMoves.first[node] ? -1 : +1;
  return stateAndMoves_t(next_state, next_moves);
} 


//For debugging purposes
string printvectorint(const vector<int> &vect){
  string s = "";
  for (auto i: vect) s+= to_string(i) + " ";
  return s;
}


/* INPUT: the Mobius values of the nodes in a DAG.
   OUTPUT: the number of states in any winning sequence.
 */
int total_length(const vector<int> &mobius){
  int ltot = 1;
  for (int val: mobius) ltot += abs(val);
  return ltot;
}


/* INPUT: the ancestors and descendent relation of a DAG and the Mobius values.
   OUTPUT: <true, sequence> if we can win the game on this dag, where sequence[i] is the state of the game after the i-th move.
           <false, emptylist> if we cannot win the game on this dag.
   Comment: to play we actually only need the ancestor relation, but we use the descendent relation in the optimization will_be_stuck().
 */
pair<bool, list<dynamic_bitset<>>> play(const vector<dynamic_bitset<>> &anc, const vector<dynamic_bitset<>> &desc, const vector<int> &mobius){
  //Some initializations
  int ltot = total_length(mobius);
  dynamic_bitset<> initial_state(anc.size());
  vector<int> initial_moves(anc.size(), 0);
  stateAndMoves_t initial_stateAndMoves = stateAndMoves_t(initial_state, initial_moves);
  unordered_set<dynamic_bitset<>> seen;           //the states we have seen during the BFS.
  map <dynamic_bitset<>, dynamic_bitset<>> pred;  //to know where a state comes from, in order to be able to reconstruct the winning sequence.
  deque<pair<int, stateAndMoves_t>> queue;
  seen.insert(initial_state);
  queue.push_front(pair<int, stateAndMoves_t>(1,initial_stateAndMoves));

  //BFS search on the graph of the states
  while (!queue.empty()){
    pair<int, stateAndMoves_t> lenAndStateAndMoves = queue.front(); queue.pop_front();
    int l = lenAndStateAndMoves.first;//the number of states in a sequence from the initial state to the state in lenAndStateAndMoves.second.
    stateAndMoves_t stateAndMoves = lenAndStateAndMoves.second;
    if ( l == (ltot/2+1) && seen.count(~stateAndMoves.first)){//Here we have not reached target, but we have already seen the flip of the current state, so we know that
                                                              //we can win. This cuts the search space by two.
                                                              //Note: I could use ltot/2 instead of ltot/2+1: this would work *except* when the DAG has only one node,
                                                              //so I leave the +1
      list<dynamic_bitset<>> winning_sequence = reconstruct_winning_sequence(stateAndMoves.first, pred);
      return pair<bool, list<dynamic_bitset<>>>(true, winning_sequence); 
    }
    if (l <= ltot/2){//Because trust me
      for (int node = 0; node < static_cast<int>(anc.size()); ++node){
        if (legal_move(stateAndMoves.first, anc[node])
            && move_is_mobius_compliant(stateAndMoves.first[node], mobius[node], stateAndMoves.second[node])
            && !will_be_stuck(stateAndMoves.first, node, anc, desc, mobius, stateAndMoves.second)){
          stateAndMoves_t next = next_stateAndMoves(stateAndMoves, node, anc[node]); 
          if (!seen.count(next.first)) {
            queue.push_back(pair<int, stateAndMoves_t>(l+1, next));
            seen.insert(next.first);
            pred[next.first] = stateAndMoves.first;
          }
        }
      }
  }
  }

  //Here we know that the DAG is losing, since no legal Mobius-compliant move leads to a winning sequence.
  return pair<bool, list<dynamic_bitset<>>>(false, list<dynamic_bitset<>>());
}


/* INPUT: ...
   OUTPUT: draws this state of the game in file "tmp/filename.png".
 */
void draw_for_DAG(const dag_t &dag, const dynamic_bitset<> &state, const vector<int> &mobius, const string &suffix){
  string dotcode = dag_to_dot_for_DAG(dag, state, mobius);
  ofstream dotfile("tmp/state_" + suffix + ".dot");
  dotfile << dotcode;
  dotfile.close();
  system(("dot -Tpng -o tmp/state_" + suffix + ".png" + " tmp/state_"+ suffix + ".dot").c_str());
}


/* INPUT: the DAG UNIONS(spf) for an SPF on a base set of k+1 elements,
          state of the play, Mobius values, original names of the elements.
   OUTPUT: draws this state of the game in file "tmp/filename.png".
 */
void draw_for_SPF(int k, const dag_t &dag, const dynamic_bitset<> &state, const vector<int> &mobius, const map<int,myint> &original_names, const string suffix){
  string dotcode = dag_to_dot_for_SPF(k, dag, state, mobius, original_names);
  ofstream dotfile("tmp/state_" + suffix + ".dot");
  dotfile << dotcode;
  dotfile.close();
  system(("dot -Tpng -o tmp/state_" + suffix + ".png" + " tmp/state_"+ suffix + ".dot").c_str());
}


void draw_winning_sequence_for_DAG(const dag_t &dag, vector<int> &mobius, list<dynamic_bitset<>> &winning_sequence){
  dynamic_bitset<> state;
  int i = 0;
  int number_of_digits = to_string(winning_sequence.size()).length(); // This is a bit ugly, by I use this to know how much padding is needed in COUCOU
  list <dynamic_bitset<>> :: iterator it; 
  for(it = winning_sequence.begin(); it != winning_sequence.end(); ++it, ++i){
    state = *it;
    // COUCOU I want to padd the numbers i so that they all have the same number of digits,
    // because I might want the files to be in lexicodagical order,
    // for instance to run "convert -delay 150 -loop 0 *.png animation.gif" (this is the script tmp/create_gif.sh)
    stringstream ss;
    ss << setw(number_of_digits) << setfill('0') << i;
    string suffix = ss.str();
    draw_for_DAG(dag, state, mobius, suffix);
  }
}


void draw_winning_sequence_for_SPF(int k, const dag_t &dag, const vector<int> &mobius, const list<dynamic_bitset<>> &winning_sequence, const map<int,myint> &original_names){
  dynamic_bitset<> state;
  int i = 0;
  int number_of_digits = to_string(winning_sequence.size()).length(); // This is a bit ugly, by I use this to know how much padding is needed in COUCOU
  list <dynamic_bitset<>> :: iterator it; 
  for(auto it = winning_sequence.begin(); it != winning_sequence.end(); ++it, ++i){
    state = *it;
    // COUCOU I want to padd the numbers i so that they all have the same number of digits,
    // because I might want the files to be in lexicodagical order,
    // for instance to run "convert -delay 150 -loop 0 *.png animation.gif" (this is the script tmp/create_gif/sh)
    stringstream ss;
    ss << setw(number_of_digits) << setfill('0') << i;
    string suffix = ss.str();
    draw_for_SPF(k, dag, state, mobius, original_names, suffix);
  }
}


/* INPUT: a DAG.
   OUTPUT: 1- compute what is needed to draw it and to play.
           2- draw it.
           3- play on it.
           4- Prints "WIN" if it wins, and puts in directory tmp the pngs of the states state_0.png, state_1.png ... until the winning state.
              Else, prints "LOST".
 */
void play_and_draw_on_this_DAG(const dag_t &dag){
  cout << "Computing the descendants and ancestors relations...\n";
  dag_t inv = inverse_dag(dag);
  vector<dynamic_bitset<>> descendents = compute_transitive_closure(inv);
  vector<dynamic_bitset<>> ancestors = compute_transitive_closure(dag);
  vector<int> mobius = compute_mobius(descendents);
  dynamic_bitset<> all_off_state(dag.size());
  cout << "Done. I will now clean tmp/ and draw the DAG in tmp/state_init.png so that you can look at it while it is playing.\n";
  system("rm tmp/*.dot tmp/*.png tmp/*.gif");
  draw_for_DAG(dag, all_off_state , mobius, "init");

  cout << "Done. Now playing...\n";
  pair<bool, list<dynamic_bitset<>>> result = play(ancestors, descendents, mobius);
  if (!result.first){
    cout << "LOST :-( \n";
    return;
  }
  cout << "WIN :-) I will now draw the winning sequence in tmp/state_*.png\n";
  list<dynamic_bitset<>> winning_sequence = result.second;
  draw_winning_sequence_for_DAG(dag, mobius, winning_sequence);
  cout << "Done.\n";
}


void play_and_draw_on_DAG(const string &filepath){
  ifstream ifile(filepath);
  if (!ifile){
    cout << "The file " << filepath << " does not exist.\n\n";
    return;
  }
  dag_t dag = read_next_dag(ifile);
  ifile.close();
  play_and_draw_on_this_DAG(dag);
}


/* INPUT: a filepath that stores some DAGs, and integer
   OUTPUT: the nb-th DAG stored in the file
 */
dag_t read_random_DAG(string filepath, int nb){
  ifstream ifile (filepath); 
  string line, tmp;
  //skip the lines until the nb-th graph
  for (int s=1; s<nb; ++s){
    getline(ifile, line);
    while (line.length() != 0) getline(ifile, line);
  }
    
  getline (ifile,line);
  dag_t dag(stoi(line));
  int source_node;
  while ( line.length() != 0 ){
    getline (ifile,line);
    vector<int> neighbors;
    istringstream iss(line);
    iss >> tmp;
    source_node = stoi(tmp);
    for(string s; iss >> s; ){
      neighbors.push_back(stoi(s));
    }
    dag[source_node] = neighbors;
  }
  ifile.close();
  return dag;
}


/* INPUT: a file storing some DAGs (generated by the code in directory random_DAGs)
   OUTPUT: the number of DAGs stored in this file (which is just the number of empty lines)
 */
int get_nb_DAGs(const string filepath){
  ifstream ifile (filepath); 
  string line;
  int count = 0;
  while (getline(ifile, line)){
    if (line.length() == 0) ++count;
  }
  return count;
}


/* INPUT: a file where some DAGs are stored (generated by the code in directory random_DAGs), an integer.
   OUTPUT: plays and draw on the nb-th DAG stored in this file.
 */
void play_and_draw_on_one_random_dag(const string filepath, int nb){
  ifstream ifile(filepath);
  if (!ifile){
    cout << "The file " << filepath << " does not exist.\n\n";
    return;
  }
  int nb_DAGs = get_nb_DAGs(filepath);
  if (nb < 1){cout << "nb must be > 0\n\n"; return;}
  if (nb > nb_DAGs){cout << "There are only "  << nb_DAGs << " stored in this file\n"; return;}
  string line;
  //skip the lines until the nb-th graph
  for (int s=1; s<nb; ++s){
    getline(ifile, line);
    while (line.length() != 0) getline(ifile, line);
  }
  
  dag_t dag = read_next_dag(ifile);
  ifile.close();
  play_and_draw_on_this_DAG(dag);
}


/* INPUT: a file where some DAGs are stored (generated by the code in directory random_DAGs).
   OUTPUT: plays and draw on all the DAGs stored in this file.
 */
void play_and_draw_on_all_random_dag(const string filepath){
  ifstream ifile(filepath);
  if (!ifile){
    cout << "The file " << filepath << " does not exist.\n\n";
    return;
  }
  int nb_DAGs = get_nb_DAGs(filepath);
  cout << "There are "  << nb_DAGs << " stored in this file\n";
  int countlost = 0;
  string lostDAGs = "";
  dag_t dag, inv;
  vector<dynamic_bitset<>> descendents, ancestors;
  vector<int> mobius;
  for(int l=1; l <= nb_DAGs; ++l){
    dag = read_next_dag(ifile);
    inv = inverse_dag(dag);
    descendents = compute_transitive_closure(inv);
    ancestors = compute_transitive_closure(dag);
    mobius = compute_mobius(descendents);

    //cout << "Playing on the " << l << "-th DAG...\n";
    pair<bool, list<dynamic_bitset<>>> result = play(ancestors, descendents, mobius);
    if (!result.first){
      cout << "LOST\n";
      countlost++;
      lostDAGs += to_string(l) + "-";
    }
    if (l % 1000 == 0) cout << l << "DAGs treated (lost " << countlost << ")\n";
  }
  cout << "Finished. Lost on " << countlost << " DAGs out of " << nb_DAGs << ": " << lostDAGs;
  ifile.close();
}


/* INPUT: a filestream storing SPFs.
   OUTPUT: the next SPF stored in the file.
 */
vector<myint> get_next_SPF(ifstream &ifile){
  string line;
  getline(ifile,line);
  vector<myint> spf;
  istringstream iss(line);
  for(string s; iss >>s; ){
    spf.push_back(static_cast<myint>(stoi(s)));
  }
  return spf;
}


// Number of lines in the file "generated/sperner/all_ineq_nondegenerate/k/l".
int number_lines(int k, int l){
  int number_of_lines = 0;
  ifstream ifile ("generated/sperner/all_ineq_nondegenerate/" + to_string(k) + "/" + to_string(l));
  string line;
  while (std::getline(ifile, line)) ++number_of_lines;
  return number_of_lines;
}


/* INPUT: an SPF spf.
   OUTPUT: a DAG on nodes 0,1,2,... isomorphic to UNIONS(spf) (see the README), and the mapping from the sets to the new node ids.

   Comment: in the comments of this function, let us call H this Hasse diagram.
            The idea is to explore H via a BFS-like traversal starting from the leaves (the sets in the spf),
            and to assign a fresh name in a continuous range starting from 0 to all the nodes that we discover.
            I need to rename the nodes of H in this way because the functions to play on an
            arbitrary DAG assume that the nodes of this DAG are a continuous range of ints starting from 0.
 */
pair<dag_t, map<int,myint>> dag_and_map_from_SPF(const vector<myint> &spf){
  unordered_set<myint> current(spf.begin(),spf.end());//note to self: there is a reason why I use unordered_set and not directly vector (too annoying to explain)
  map<int, unordered_set<int>> dag_as_map;            //this will hold the adjacency lists of H. I use a map instead of dag_t because
                                                      //I don't know in advance the size of the DAG, and I don't want to traverse it twice.
  map<myint,int> names;       //this is the map holding the renaming of the nodes of H.
  map<int,myint> reverse_map; //and this is the map from the renamings to the original sets
  int fresh_name = 0;
  for (auto &set: spf){
    reverse_map[fresh_name] = set;
    names[set] = fresh_name++;
    dag_as_map[names[set]] = unordered_set<int>();
  }

  //At the i-th iteration of this loop, current contains all the nodes of H whose minimal distance from some leaf of H equals i-1.
  while (!current.empty()){
    unordered_set<myint> next_current;
    for (myint t: current){
      for (myint set: spf){
        if ( !names.count(t|set)){//true if we are just discovering (t|set)
          reverse_map[fresh_name] = (t|set);
          names[t|set] = fresh_name++;
          dag_as_map[names[t|set]] = unordered_set<int>();
          next_current.insert(t|set);
        }
        //Here we check that (t|set) is a parent of t. Note that all edges t -> t' in H are such that t' = t|set for some set in the spf,
        //but that for a node t in H and set in spf, (t|set) is not necessarily a parent of t...
        bool is_parent = true;
        if ((t|set) == t) is_parent = false;//The condition means that t|set is strictly above t
        for (myint set2: spf){ 
        //The following condition is true if t|set2 is strictly in between t and t|set.
        // Indeed, the first clause is that t|set2 is strictly above t, the second clause says that t|set2 is below t|set,
        //and the third clause says that t|set2 is strictly below t|set.
        if (  ( (t|set2) != t ) && ( (t|set|set2) == (t|set) ) && ( (t|set2) != (t|set) )   ) is_parent = false;
        }
        if (is_parent) dag_as_map[names[t]].insert(names[t|set]);
      }
    }
    current = next_current;
  }

  //Now convert dag_as_map to a dag_t
  dag_t dag(fresh_name, vector<int>());
  for (pair<int, unordered_set<int>> elem : dag_as_map){
    for (auto &neigh: elem.second) dag[elem.first].push_back(neigh);
  }
  
  return pair<dag_t, map<int,myint>>(dag, reverse_map);
}


/* INPUT: k,l,j
   OUTPUT: 1- reads the SPF in stored in line j of the file "generated/sperner/all_ineq_nondegenerate/k/l" 
           2- computes the things we need to play and draw
           3- draw UNIONS(spf)
           4- play on the DAG
           5- if it wins, print "WON" and draw in tmp/ the winning sequence. Else, prints "LOST"
 */
void play_and_draw_on_one_SPF(int k, int l, int j){
  //Some safeguards.
  ifstream ifile ("generated/sperner/all_ineq_nondegenerate/" + to_string(k) + "/" + to_string(l));
  if (!ifile){
    cout << "The file generated/sperner/all_ineq_nondegenerate/" + to_string(k) + "/" + to_string(l) << " does not exist.\n\n";
    return;
  }
  if (j < 1){cout << "Line number must be > 0\n"; return;}
  int nb_lines=number_lines(k,l);
  if (j > nb_lines ){
    cout << "The file generated/sperner/all_ineq_nondegenerate/" + to_string(k) + "/" + to_string(l) << " has only " << nb_lines << " lines.\n\n";
    return;
  }
  
  //Fetch the SPF and show it to user.
  string line;
  for(int l=1; l < j; ++l) getline(ifile,line);
  vector<myint> spf = get_next_SPF(ifile);
  ifile.close();
  cout << "=======> You want to play on the SPF spf = ";
  for (auto set: spf) cout << set << " ";
  cout << "(as bitsets: ";
  vector<dynamic_bitset<>> spf_as_bitsets(0);
  for (auto set:spf) spf_as_bitsets.push_back(dynamic_bitset<>(k+1,set));
  for (auto set: spf_as_bitsets) cout << set << " ";
  cout << ")\n";
  
  //Compute the things we need to play and draw
  cout << "Constructing the DAG UNIONS(spf)...\n";
  pair<dag_t, map<int,myint>> dagAndMap = dag_and_map_from_SPF(spf);
  dag_t dag = dagAndMap.first;
  map<int,myint> original_names = dagAndMap.second;
  cout << "Computing the descendants and ancestors relations...\n";
  dag_t inv = inverse_dag(dag);
  vector<dynamic_bitset<>> descendents = compute_transitive_closure(inv);
  vector<dynamic_bitset<>> ancestors = compute_transitive_closure(dag);
  vector<int> mobius = compute_mobius(descendents);
  dynamic_bitset<> all_off_state(dag.size());

  //Draw the DAG
  cout << "Done. I will now clean tmp/ and draw the DAG in tmp/state_init.png so that you can look at it while it is playing.\n";
  system("rm tmp/*.dot tmp/*.png tmp/*.gif");
  draw_for_SPF(k, dag, all_off_state , mobius, original_names, "init");


  //Play and draw if win
  cout << "Done. Now playing...\n";
  pair<bool, list<dynamic_bitset<>>> result = play(ancestors, descendents, mobius);
  if (!result.first){
    cout << "LOST :-( \n";
    return;
  }
  cout << "WIN :-) I will now draw the winning sequence in tmp/state_*.png\n";
  list<dynamic_bitset<>> winning_sequence = result.second;
  draw_winning_sequence_for_SPF(k, dag, mobius, winning_sequence, original_names);
  cout << "Done.\n";
}


/* INPUT: ... 
   OUTPUT: play on all the SPFs stored in file "generated/sperner/all_ineq_nondegenerate/k/l", starting from line start_line. Stops as soon as it finds one on which we lose, and print the line number.
 */
void play_on_all_SPFs(int k, int l, int start_line){
  cout << "=================\n You want to play on all the SPFs stored from line " << start_line << " in file \"generated/sperner/all_ineq_nondegenerate/" <<k << "/" << l <<"\"\n==================\n";
  //Some safeguards.
  ifstream ifile ("generated/sperner/all_ineq_nondegenerate/" + to_string(k) + "/" + to_string(l));
  if (!ifile){
    cout << "The file generated/sperner/all_ineq_nondegenerate/" + to_string(k) + "/" + to_string(l) << " does not exist.\n\n";
    return;
  }
  if (start_line < 1){cout << "Starting line must be > 0\n"; return;}
  int nb_lines=number_lines(k,l);
  cout << "There are " << nb_lines << " SPFs in this file.\n";
  if (start_line > nb_lines) return;
  string line;
  for (int j=1; j<start_line; ++j) getline(ifile,line);
  for (int j = start_line; j <= nb_lines; ++j){
    //Fetch the SPF.
    vector<myint> spf = get_next_SPF(ifile);
    
    //Compute the things we need to play.
    dag_t dag = dag_and_map_from_SPF(spf).first;
    dag_t inv = inverse_dag(dag);
    vector<dynamic_bitset<>> descendents = compute_transitive_closure(inv);
    vector<dynamic_bitset<>> ancestors = compute_transitive_closure(dag);
    vector<int> mobius = compute_mobius(descendents);
  
    //Now playing.
    cout << "Playing on the SPF line " << j << " of generated/sperner/all_ineq_nondegenerate/" << k << "/" << l <<"\n";
    pair<bool, list<dynamic_bitset<>>> result = play(ancestors, descendents, mobius);
    if (!result.first){
      cout << "LOST on the SPF in line " << (j-1) << "\n";
      return;
    }
  if (j % 1000 == 0) cout << "I have treated " << j << " SPFs. Continuing...\n";
  }
  cout << "WON EVERYTHING\n\n";
  return;
}


int main(int arc, char** argv){

  //Uncomment only one:

  //#define PLAY_AND_DRAW_ON_ONE_DAG
  //#define PLAY_AND_DRAW_ON_ONE_SPF
  //#define PLAY_ON_ALL_SPFS
  //#define PLAY_ON_ALL_SPFS_FROM_LINE
  //#define PLAY_AND_DRAW_ON_ONE_RANDOM_DAG
  #define PLAY_AND_DRAW_ON_ALL_RANDOM_DAG
  

  #ifdef PLAY_AND_DRAW_ON_ONE_DAG
  /*
    Use this to 1- read a DAG, 2- draw it, 3- play on it, and then 4- draw the winning sequence if it is winning.
     (drawings in tmp/).
  */
  play_and_draw_on_DAG(argv[1]);
  #endif

  #ifdef PLAY_AND_DRAW_ON_ONE_SPF
  /* 
    Use this to play on the DAG generated by the Sperner family (SPF) stored in
    line argv[3] of the file
    "generated/sperner/all_ineq_nondegenerate/argv[1]/argv[2]". If the
    game is won, this draws the winning sequence (in tmp/).
 */
  play_and_draw_on_one_SPF(stoi(argv[1]), stoi(argv[2]), stoi(argv[3]));
  #endif


  #ifdef PLAY_ON_ALL_SPFS
  /*
    Use this to play the game on all the DAGs that are generated by the Sperner
    families stored in the file "generated/sperner/all_ineq_nondegenerate/argv[1]/argv[2]".
    This does not draw the winning strategies. It stops as soon as it finds an
    SPF on which we lose, and prints that SPF to stdout.
 */
  play_on_all_SPFs(stoi(argv[1]), stoi(argv[2]), 1);
  #endif

  #ifdef PLAY_ON_ALL_SPFS_FROM_LINE
  /*
    Use this to play the game on all the DAGs that are generated by the Sperner
    families stored from line argv[3] in the file "generated/sperner/all_ineq_nondegenerate/argv[1]/argv[2]".
    This does not draw the winning strategies. It stops as soon as it finds an
    SPF on which we lose, and prints that SPF to stdout.
 */
  play_on_all_SPFs(stoi(argv[1]), stoi(argv[2]), stoi(argv[3]));
  #endif


  /*
    Use this to play on the argv[1]-th DAG stored in the file argv[1]. This
    file should have been generared by the code in the directory random_DAGs.
   */
  #ifdef PLAY_AND_DRAW_ON_ONE_RANDOM_DAG
  play_and_draw_on_one_random_dag(string(argv[1]), stoi(argv[2]));
  #endif

  /*
    Use this to play on all the DAGs stored in the file argv[1]. This
    file should have been generared by the code in the directory random_DAGs.
   */

  #ifdef PLAY_AND_DRAW_ON_ALL_RANDOM_DAG
  play_and_draw_on_all_random_dag(string(argv[1]));
  #endif
}


