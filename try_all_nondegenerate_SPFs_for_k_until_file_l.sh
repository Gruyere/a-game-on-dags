#!/bin/bash

if [ $1 -lt 1 ] || [ $2 -lt 1 ]; then
  echo "k and l must be > 0. Read the README."
fi

for i in $(seq 1 $2); do
    ./a.out $1 $i;
done
